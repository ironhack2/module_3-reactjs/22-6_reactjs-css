import avatar from './assets/images/48.jpg';

const style ={
  container:{
   width: "700px",
    margin: "0 auto",
    textAlign: "center",
    border: "1px solid #ddd",
    padding: "40px 25px",
    marginTop: "100px",
    backgroundColor:"antiquewhite"
    },

    avatar:{
      margin: "-90px auto 30px",
      width: "100px",
      borderRadius: "50px",
      objectFit: "cover",
      marginBottom: "30px"
    },

    quote:{
      fontSize:"17px",
      marginBottom: "25px",
      fontWeight: "300",
      fontSize:"1.375rem",
      fontStyle:"italic"
    },

    name:{
      fontWeight:"600",
      fontSize: "1rem",
      color:"blue"
    },

    job:{
      fontWeight: "400",
      opacity: "0.8",
      color: "rgb(191, 12, 12)"
    }
}

function App() {
  return (
    <div style={style.container}>
            <div>
                 <img style={style.avatar} src={avatar} alt="Tammy Steven" />
            </div>

            <div style={style.quote}>
                  This is one of the best developer blogs on the planet! I read it daily to improve my skills.
            </div>

            <div>
                 <span style={style.name}>
                          Tammy stevens
                 </span>
                 <span style={style.job}>
                        &nbsp;* Front End Developer
                 </span>
            </div>
    </div>
  );
}

export default App;
